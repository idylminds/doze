#ifndef _DOZE_DOT_H_
#define _DOZE_DOT_H_

#include "Arduino.h"

// prescalar values
#define DURATION_16MS  0
#define DURATION_32MS  1
#define DURATION_64MS  2
#define DURATION_128MS 3
#define DURATION_250MS 4
#define DURATION_500MS 5
#define DURATION_1S    6
#define DURATION_2S    7
#define DURATION_4S    8
#define DURATION_8S    9

class Doze
{
 public:
  Doze(uint8_t prescalar, uint8_t cycles = 1);
  bool pause();
};

#endif // _DOZE_DOT_H_
