#include "Arduino.h"
#include "Doze.h"

#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

volatile uint8_t _cyclesRemaining;
uint8_t _cycles;

// Executed when watchdog timed out.
ISR(WDT_vect)
{
  --_cyclesRemaining;
}

Doze::Doze(uint8_t prescalar, uint8_t cycles)
{
  prescalar = min(9,prescalar);

  // clear the reset flag
  MCUSR &= ~(1<<WDRF);

#if defined( __AVR_ATtiny25__ ) || defined( __AVR_ATtiny45__ ) || defined( __AVR_ATtiny85__ )

  // set WDCE allowing updates for 4 clock cycles
  WDTCR |= (1<<WDCE) | (1<<WDE);
  WDTCR = 0;

  // compute the timeout from the prescalar
   if (prescalar & 1)
     WDTCR |= (1<<WDP0);

   if (prescalar & 2)
     WDTCR |= (1<<WDP1);

   if (prescalar & 4)
     WDTCR |= (1<<WDP2);

   if (prescalar & 8)
     WDTCR |= (1<<WDP3);

  // enable the WD interrupt
  WDTCR |= _BV(WDIE);

#else
  // set WDCE allowing updates for 4 clock cycles
  WDTCSR |= (1<<WDCE) | (1<<WDE);
  WDTCSR = 0;

  // compute the timeout from the prescalar
  if (prescalar & 1)
     WDTCSR |= (1<<WDP0);

   if (prescalar & 2)
     WDTCSR |= (1<<WDP1);

   if (prescalar & 4)
     WDTCSR |= (1<<WDP2);

   if (prescalar & 8)
     WDTCSR |= (1<<WDP3);

  // enable the WD interrupt
  WDTCSR |= _BV(WDIE);

#endif

  _cycles = _cyclesRemaining = cycles;
}

bool Doze::pause()
{
  // turn off ADC  
  ADCSRA &= ~(1<<ADEN);

  // disable the analog comparator                  
  // ACSR |= _BV(ACD);

  // turn off the brown-out detector
  // MCUCR |= _BV(BODS) | _BV(BODSE);

  // sleep in power down mode 
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  // disable peripherals
  sleep_enable();

  // enter sleep mode
  sleep_mode();

  // woken up
  sleep_disable();

  // re-enable the peripherals
  power_all_enable();

  // enable ADC
  ADCSRA |= (1<<ADEN);

  if (_cyclesRemaining <= 0)
  {
      _cyclesRemaining = _cycles;
      return true;
  }

  return false;
}



